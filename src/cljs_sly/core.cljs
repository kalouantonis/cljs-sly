(ns cljs-sly.core
  (:require [cljs-sly.math.core :as math]))

(enable-console-print!)

(println "Hello, I come bearing pie:" math/pi)

;; define your app data so that it doesn't get over-written on reload
(defonce app-state (atom {:text "Hello world!"}))

(defn on-js-reload [])
