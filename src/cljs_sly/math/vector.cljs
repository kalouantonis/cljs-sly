(ns cljs-sly.math.vector)

(defrecord Vector2 [x y])
(defrecord Vector3 [x y z])
(defrecord Vector4 [x y z w])

(defn make-vector2
  [x y]
  (Vector2. x y))

(defn make-vector3
  [x y z]
  (Vector3. x y z))

(defn make-vector4
  [x y z w]
  (Vector4. x y z w))

;; I'm not sure if this is a great idea
(defprotocol IVectorable
  (vfn [f])
  (vmap [f v]))

;; TODO: Clean this up
(extend-protocol IVectorable
  Vector2
  (vfn [f]
    (fn [{:keys [x1 y1]} {:keys [x2 y2]}]
      (Vector2. (f x1 x2) (f y1 y2))))
  (vmap [f v] (Vector2. (f (vx v)) (f (vy v))))
  Vector3
  (vfn [f]
    (fn [{:keys [x1 y1 z1]} {:keys [x2 y2 z2]}]
      (Vector3. (f x1 x2) (f y1 y2) (f z1 z2))))
  (vmap [f v] (Vector3. (f (vx v)) (f (vy v)) (f (vz v))))
  Vector4
  (vfn [f]
    (fn [{:keys [x1 y1 z1 w1]} {:keys [x2 y2 z2 w2]}]
      (Vector4. (f x1 x2) (f y1 y2) (f z1 z2) (f w1 w2))))
  (vmap [f v] (Vector4. (f (vx v)) (f (vy v)) (f (vz v)) (f (vw v)))))

(defn- foldr
  "Reverse fold."
  [f val coll]
  (reduce #(f %2 %1) val (reverse coll)))

(defn v+ [& vecs] (reduce (vfn +) 0 vecs))

(defn v-
  ([v] (v- 0 v))
  ([v & v*]
   (foldr (let [- (vfn -)]
            (fn [prev v]
              (- v prev)))
          v v*)))

(defn v* [& vecs]
  (reduce (vfn *) 1 vecs))
