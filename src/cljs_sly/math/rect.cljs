(ns cljs-sly.math.rect
  (:require [cljs-sly.math.vector :as vector]))

(defrecord Rect [x y w h])

(defn make-rect
  ([position size]
   (make-rect (:x position) (:y position)
              (:x size) (:y size)))
  ([x y w h] (Rect. x y w h))  )

(def null-rect (make-rect 0 0 0 0))

(defn rect-right [rect]
  (+ (:x rect) (:width rect)))

(defn rect-left [rect] (:x rect))
(defn rect-bottom [rect] (:y rect))

(defn react-top [rect]
  (+ (:y rect) (:height rect)))

(defn rect-position [rect]
  (vector/make-vector2 (:x rect)
                       (:y rect)))

(def rect-bottom-left rect-position)
