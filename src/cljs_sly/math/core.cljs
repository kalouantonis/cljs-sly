(ns cljs-sly.math.core)

(def pi (.-PI js/Math))

(defn degrees->radians
  "Convert ANGLE in degrees to radians."
  [angle]
  (* angle (/ pi 180)))

(defn radians->degrees
  "Convert ANGLE in radians to degrees."
  [angle]
  (* angle (/ 180 pi)))

(defn sin-degrees
  "Compute the sin of ANGLE expressed in degrees."
  [angle]
  (->> angle
      (degrees->radians)
      (.sin js/Math)))

(defn cos-degrees
  "Compute the cosine of ANGLE expressed in degrees."
  [angle]
  (->> angle
       (degrees->radians)
       (.cos js/Math)))

(defn tan-degrees
  "Compute the tangent of ANGLE expressed in degrees."
  [angle]
  (->> angle
       (degrees->radians)
       (.tan js/Math)))

(defn atan-degrees
  "Compute the arctangent in degrees of the coordinates Y and X."
  [y x]
  (radians->degrees (.atan2 js/Math y x)))

(defn cotan
  "Return the cotangent of Z."
  [z]
  (/ 1 (.tan js/Math z)))

(defn clamp
  "Restrict X to the range defined by MIN and MAX. Assumes that MIN is
actually less than MAX."
  [min max x]
  (cond
    (< x min) min
    (> x max) max
    :else x))

(defn linear-scale [min max a b n]
  "Map a number N in the range [MIN,MAX] to the range [A,B]."
  (+ a
     (/ (* (- b a) (- n min))
        (- max min))))

(defn half [x]
  (/ x 2))

(defn square [x]
  (* x x))

(defn make-lerp
  "Return a new procedure that accepts three arguments: START, END,
and ALPHA.  The returned procedure uses the procedures + and * to
linearly interpolate a value between START and END.  ALPHA should
always be in the range [0, 1]."
  [+ *]
  (fn [start end alpha]
    (+ (* start (- 1 alpha))
       (* end alpha))))

(def lerp (make-lerp + *))

(defn modulo*
  "Return the remainder of X / Y.  X and Y may be exact integers,
rational numbers, or inexact numbers."
  [x y]
  (- x (* (.floor js/Math (/ x y)) y)))
